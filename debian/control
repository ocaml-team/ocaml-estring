Source: ocaml-estring
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Dmitrijs Ledkovs <dmitrij.ledkov@ubuntu.com>
Build-Depends:
 debhelper (>= 9),
 dh-ocaml,
 libfindlib-ocaml-dev,
 liboasis-ocaml-dev (>= 0.4.5),
 ocaml-findlib,
 camlp4,
 ocaml-nox
Standards-Version: 3.9.6
Homepage: https://forge.ocamlcore.org/projects/estring/
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-estring
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-estring.git

Package: libestring-ocaml
Architecture: any
Depends:
 ${misc:Depends},
 ${ocaml:Depends},
 ${shlibs:Depends}
Provides:
 ${ocaml:Provides}
Conflicts:
 libbatteries-ocaml-dev (<< 2.0)
Replaces:
 libbatteries-ocaml-dev (<< 2.0)
Description: Estring: OCaml development platform (runtime)
 estring, which stands for `extended strings' is a syntax extension
 allowing to prefix string literals with a specifier to change their
 meaning.
 .
 This package used to be part of the batteries project.
 .
 This package contains the shared runtime libraries.

Package: libestring-ocaml-dev
Architecture: any
Depends:
 ocaml-findlib,
 ${misc:Depends},
 ${ocaml:Depends},
 ${shlibs:Depends}
Provides:
 ${ocaml:Provides}
Conflicts:
 libbatteries-ocaml-dev (<< 2.0)
Replaces:
 libbatteries-ocaml-dev (<< 2.0)
Description: Estring: OCaml development platform (development)
 estring, which stands for `extended strings' is a syntax extension
 allowing to prefix string literals with a specifier to change their
 meaning.
 .
 This package used to be part of the batteries project.
